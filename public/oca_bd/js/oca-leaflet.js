   
   var layerList = [];
   
   var layerControl = L.control.layers(null,null,{collapsed:true,autoZIndex:true});
 
   var legendDiv = '';
   var legendList = [];
   
   // fix circleMarker disappearing on zoom, as per: https://github.com/Leaflet/Leaflet/issues/2824
   L.Circle.prototype._checkIfEmpty = function () { return false; };
   
    var legendControl = L.control({position: 'bottomleft'});
    legendControl.onAdd = function (map) {

	var div = L.DomUtil.create('div', 'info legend');     
	var colorList=[];
	legendList.sort(function(a, b){ return a.cat>b.cat });

	for (var i = 0; i < legendList.length; i++) 
	{
	  if (colorList.indexOf(legendList[i].color)==-1) 
	  {
	    colorList.push(legendList[i].color);
	    div.innerHTML += '<i class="circle" style="background:' + legendList[i].color + '"></i> '+legendList[i].cat+'<br>';
	  }
	}
	if  (ejoltLayer==1) div.innerHTML += '<i class=\"small-circle\" style=\"background: orange \"></i> En análisis (ejolt) <br>';
	return div;
    };
          
   var osmLayer = L.tileLayer('{protocol}//tiles.conflictos-ambientales.net/{z}/{x}/{y}', 
   {
    protocol : window.location.protocol, 
    location : window.location.hostname, 
    attribution: '<a href="http://creativecommons.org/licenses/by-sa/3.0/">(cc-by-sa)</a> <a href="http://www.idea.unal.edu.co">OCA-IDEA-UN</a> / <a href="http://openstreetmap.org">OpenStreetMap</a>'
   }
   );
   osmLayer.addTo(map);

   
   var iconOptions = null;
   var polygonOptions = null;
   
   var geojsonMarkerOptions = {
    radius: 3,
    fillColor: "orange",
    color: "red",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
  };

  function orderLayers(layerList) {
   
    // DISABLED: if a layer does not load correctly we still want ordering to be right... 
    //if (layerList.length==layerCount) {
      //alert('All Layers loaded!!!');
      
      layerList.sort(function(a, b){ return a.prioridad-b.prioridad });
      
      for (var i=0; i < layerList.length; i++) {
        layerList[i].layer.bringToFront();
        //alert(layerList[i].prioridad);
      }
      
    //}  
  }
  
  
  function loadLayer(geojson,layerName,layerStyle,layerPrioridad,layerLabel) {
  
      var layerColor='darkgrey';
      var strokeColor='lightgrey';
             
      if (layerStyle!=null) {

	// set style colors
	if (layerStyle['fillColor']!=null) 
	{
	  layerColor=layerStyle['fillColor'];
	  if (layerStyle['color']!=layerStyle['fillColor']) strokeColor=layerStyle['color'];
	}
	else if (layerStyle['color']!=null) layerColor=layerStyle['color']; 

	// set icon style
	if (layerStyle.icon!=null) {	  
	  iconOptions = { 'icon': L.icon({iconUrl: '/oca_bd/img/'+layerStyle.icon})	};
	}
	else iconOptions = null;      

	// set polygon style  
	if (layerStyle.polygon!=null) {
	  
	  polygonOptions = { 'polygon': layerStyle.polygon,'style':layerStyle.style };
	  
	}
	else polygonOptions = null;    
	      
      }
	
	
      var geojsonLayer = L.geoJson(geojson, {
      
	  style: function(feature) {
            if (feature.color!=null) 
            { 	                      
	      return {radius: 5,fillColor: feature.color,color: "black", weight: 1,opacity: 1, fillOpacity: 0.8};	    
	    }	   
	  },
	  
	  pointToLayer: function (feature, latlng) {

	    // icon marker
	    if (iconOptions!=null) return L.marker(latlng, iconOptions);
	    // polygon marker
	    else if (polygonOptions!=null) 
	    {
	      polyCoords=[];
	      var len=polygonOptions.polygon.length;
	      for (var i = 0; i < len; i+=2) 
	      { 
	        var proj=L.Projection.Mercator.unproject(L.point(polygonOptions.polygon[i],polygonOptions.polygon[i+1]));
	        polyCoords.push(new L.LatLng(latlng.lat+proj.lat, latlng.lng+proj.lng));
	      }	
	      return L.polygon(polyCoords,polygonOptions.style);
	    }
	    // default is a simple circle marker
	    else 
	      return L.circleMarker(latlng, geojsonMarkerOptions);
	  },        
	
	  onEachFeature: function (feature, layer) {
	    
	    if (feature.properties[layerLabel]!=null) 
	    {
	      var label='<span style="color:black">'+feature.properties[layerLabel]+'</span>';	      
	      if (feature.properties.link!=null) 
	      {	     
	        label='<a target="_blank" href=\"'+feature.properties.link+'\">'+feature.properties[layerLabel]+'</a>';
	      }
	      
	      // popup with on click event
	      layer.bindPopup(label);
	      
	    }

	    // if feature has color attrib: add it to legendList  
	    if (feature.color!=null) legendList.push({color:feature.color,cat:feature.cat});

	  }
      });
      
      if (layerStyle!=null) {
        if (layerStyle['angle']!=null) 
        {      
          var thesestripes=new L.StripePattern(layerStyle);
          thesestripes.addTo(map);         
	  geojsonLayer.setStyle({fillPattern:thesestripes, fillColor: layerStyle['fillColor'], fillOpacity: layerStyle['fillOpacity'],color:layerStyle['color'],weight:layerStyle['weight']});	  
	}
	else geojsonLayer.setStyle(layerStyle);     
      }
      //else geojsonLayer.setStyle(geojsonMarkerOptions);      
      
      geojsonLayer.addTo(map);
      
      //geojsonLayer.bindPopup('<a href=\"'+'link here'+'\">'+layerName+'</a>');
	
      //geojsonLayer.bringToFront();
      
      // switched layer order to allow for drawing only the first 5 layers
      //geojsonLayer.bringToBack();
            
      //layerList[layerPrioridad]=geosjonLayer;
      layerList.push({layer:geojsonLayer,prioridad:layerPrioridad});
      
      orderLayers(layerList);
            
      //geojsonLayer.visible=0;
      
      //layerControl.addOverlay(geojsonLayer,'<span style="text-shadow: -0.1em 0 0.07em '+strokeColor+', 0 0.1em 0.07em '+strokeColor+', 0.1em 0 0.07em '+strokeColor+', 0 -0.1em 0.07em '+strokeColor+'; color:'+layerColor+'">'+layerName+'</span>');
      layerControl.addOverlay(geojsonLayer,'<b style="color:'+layerColor+'">'+layerName+'</b>');
      
      
      if (layerName=='Conflictos') legendControl.addTo(map);	
      
      	  
    }

  layerControl.addTo(map);


